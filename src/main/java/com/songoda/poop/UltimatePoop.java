package com.songoda.poop;

import java.io.IOException;

import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.songoda.poop.listeners.FartListener;

import ch.njol.skript.Skript;

public class UltimatePoop extends JavaPlugin {

	private static UltimatePoop instance;
	private FartListener listener;

	public void onEnable(){
		instance = this;
		saveDefaultConfig();
		this.listener = new FartListener(this);
		PluginManager pluginManager = getServer().getPluginManager();
		pluginManager.registerEvents(listener, this);
		if (pluginManager.isPluginEnabled("Skript")) {
			try {
				Skript.registerAddon(this).loadClasses("com.songoda.poop", "skript");
			} catch (IOException e) {
				Skript.exception(e, "Could not hook into UltimatePoop.");
			}
		}
		getLogger().info("has been enabled!");
	}

	public static UltimatePoop getInstance() {
		return instance;
	}
	
	public FartListener getListener() {
		return listener;
	}

}
